import { Component, OnInit } from '@angular/core';
import { LogService } from '../../services/log.service';
import { TimeLog } from '../../Models/TimeLog';
import { Subscription } from 'rxjs';
import { TimeCategoryEnum } from '../../Models/TimeCategoryEnum';
import * as moment from 'moment';
import { ChartsModule } from 'ng2-charts';
import { AnalysisService } from '../../services/analysis.service';

@Component({
  selector: 'app-data',
  templateUrl: './data.component.html',
  styleUrls: ['./data.component.less']
})
export class DataComponent implements OnInit {
  // defaults

  constructor(private logService: LogService, private analysisService: AnalysisService) { }

  ngOnInit() {
    // this.startTime = this.getStartTime(this.timeRange);
    // this.logSub = this.logService.getLogsForTimeRange(this.startTime, Date.now()).subscribe(logs => {
    //   this.logs = logs;
    }
    jumpBack() {
      this.analysisService.goBack();
    }
    jumpForward() {
      this.analysisService.goForward();
    }
}

// getTimeRange(option: TimeCategoryEnum) {
//   this.timeRange = option;
// }

// getStartTime(timeCategory: TimeCategoryEnum): number {
//   const now = new Date();
//   switch (timeCategory) {
//     case TimeCategoryEnum.Month: {
//       // beginning of month
//       return Date.UTC(now.getFullYear(), now.getMonth());
//     }
//     case TimeCategoryEnum.Year: {
//       // beginning of year
//       return Date.UTC(now.getFullYear(), 1);
//     }
//   }
// }

