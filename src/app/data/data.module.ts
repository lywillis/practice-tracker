import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DataComponent } from './data/data.component';
import { ChartsModule } from 'ng2-charts';
import { RouterModule, Routes } from '@angular/router';
import { LogService } from '../services/log.service';
import { DataButtonsComponent } from './data-buttons/data-buttons.component';
import { ChartComponent } from './chart/chart.component';
import { AnalysisService } from '../services/analysis.service';

@NgModule({
  imports: [
    CommonModule,
    ChartsModule,
    RouterModule
  ],
  declarations: [DataComponent, DataButtonsComponent, ChartComponent],
  providers: [LogService, AnalysisService]
})
export class DataModule { }
