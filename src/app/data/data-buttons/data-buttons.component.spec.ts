import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DataButtonsComponent } from './data-buttons.component';

describe('DataButtonsComponent', () => {
  let component: DataButtonsComponent;
  let fixture: ComponentFixture<DataButtonsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DataButtonsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DataButtonsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
