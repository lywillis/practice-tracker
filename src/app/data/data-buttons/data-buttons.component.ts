import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { TimeCategoryEnum } from '../../Models/TimeCategoryEnum';
import { AnalysisService } from '../../services/analysis.service';

@Component({
  selector: 'app-data-buttons',
  templateUrl: './data-buttons.component.html',
  styleUrls: ['./data-buttons.component.less']
})
export class DataButtonsComponent implements OnInit {
  currentOption: TimeCategoryEnum;
  options?: string[] = Object.keys(TimeCategoryEnum).filter(key => isNaN(Number(key)));
  constructor(private analysisService: AnalysisService) { }

  ngOnInit() {
  }
  selectTimeCategory(option: string) {
    this.currentOption = TimeCategoryEnum[option];
    this.analysisService.setTimeCategory(this.currentOption);
  }
}
