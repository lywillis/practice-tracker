import { Component, OnInit, Input, OnChanges, SimpleChanges, OnDestroy, ViewChild } from '@angular/core';
import { TimeCategoryEnum } from '../../Models/TimeCategoryEnum';
import { LogService } from '../../services/log.service';
import { Subscription } from 'rxjs';
import { TimeLog } from '../../Models/TimeLog';
import * as moment from 'moment';
import { AnalysisService } from '../../services/analysis.service';
import { Color, BaseChartDirective, ChartsModule} from 'ng2-charts';

@Component({
  selector: 'app-chart',
  templateUrl: './chart.component.html',
  styleUrls: ['./chart.component.less']
})

export class ChartComponent implements OnInit, OnDestroy {
  @ViewChild(BaseChartDirective) baseChart: BaseChartDirective;
  chartData: number[] = [];
  chartLabels: any[];
  colors: Array<Color> = [];
  chartOptions: any;
  // data: number[] = [];
  timeRange: TimeCategoryEnum = TimeCategoryEnum.Month;
  startTime: number;
  endTime: number;
  logs: TimeLog[];
  logSub: Subscription;
  analysisSub: Subscription;

  uFont = 'Raleway';
constructor(private analysisService: AnalysisService, private logService: LogService) { }

ngOnInit() {
  this.analysisSub = this.analysisService.timeCategoryStatus$.subscribe(status => {
    this.timeRange = status.timeCategory;
    this.startTime = status.startTime;
    this.endTime = status.endTime;
    this.clearChart();
    this.logSub = this.logService.getLogsForTimeRange(this.startTime, this.endTime).subscribe(logs => {
      this.logs = logs;
      this.renderChartData();
    });
  } );
}
ngOnDestroy() {
  this.analysisSub.unsubscribe();
  if (this.logSub !== undefined) {
    this.logSub.unsubscribe();
  }
}
  clearChart() {
    this.chartData = [];
    this.chartLabels = [];
    this.colors = [];
    this.chartOptions = {};
  }

  renderChartData() {
    const date = new Date(this.startTime);
    switch (this.timeRange) {
      case TimeCategoryEnum.Month: {
        AnalysisService.aggregateByDay(this.logs, date.getFullYear(), date.getMonth()).forEach(data => {
          this.chartData.push(data);
        });
        this.chartLabels = this.analysisService.getLabelsForMonth(date.getFullYear(), date.getMonth());
        break;
      }
      case TimeCategoryEnum.Year: {
        AnalysisService.aggregateByMonth(this.logs).forEach(data => {
          this.chartData.push(data);
        });
        this.chartLabels = this.analysisService.getLabelsForYear();
        break;
      }
      case TimeCategoryEnum.Week: {
        AnalysisService.aggregateByDayOfWeek(this.logs).forEach(data => {
          this.chartData.push(data);
        });
        this.chartLabels = this.analysisService.getLabelsForWeek();
        break;
      }
      case TimeCategoryEnum.Day: {
        AnalysisService.aggregateByHour(this.logs).forEach(data => {
          this.chartData.push(data);
        });
        this.chartLabels = this.analysisService.getLabelsForDay();
        break;
      }
    }
    this.chartOptions = {
      responsive: true,
      legend: {
        display: false
      },
      title : {
        display: true,
        text: this.analysisService.getChartTitle(),
        fontFamily: this.uFont
      },
      scales : {
        yAxes: [{
          ticks: {
            min: 0,
            callback: v => this.formatTime(v),
            suggestedMax: this.getTickMax(),
            stepSize: 3600 / 2,
            fontFamily: this.uFont
          },
          scaleLabel: {
            display: true,
            labelString: this.getYAxisLabel(),
            fontFamily: this.uFont
          }
        }],
        xAxes: [{
          ticks: {
            fontFamily: this.uFont
          }
        }]
      }
    };
    this.getChartColors();
  }

  getChartColors() {
    const startRed = 144;
    let startBlue = 144;
    const startGreen = 238;
    const backgroundColors = [];
    for (const bar of this.chartData) {
      if (bar !== 0) {
        const startingColor = `rgb(${startRed},${startGreen},${startBlue})`;
        backgroundColors.push(startingColor);
        startBlue += 10;
      } else {
        backgroundColors.push('transparent');
      }
    }
    this.colors.push({ backgroundColor: backgroundColors });
  }

  formatTime(v: number) {
    const maxVal = Math.max(...this.chartData);
    if (maxVal >= 3600) { // format as hours
      return (v / 3600).toFixed(2);
    }
    if (maxVal >= 60) {
      return (v / 60).toFixed(2);
  }
  return v;
}

getTickMax() {
  const maxVal = Math.max(...this.chartData);
    if (maxVal < 3600) { // format as hours
      return 3600;
    } else {
      return;
  }
}
getYAxisLabel() {
  const maxVal = Math.max(...this.chartData);
  if (maxVal >= 3600) {// hours
    return 'Total Hours';
  }
  if (maxVal >= 60) {
    return 'Total Minutes';
  }
}
}
