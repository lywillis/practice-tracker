import { TimeLog } from '../../Models/TimeLog';
import { LogService } from '../../services/log.service';
import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';
import { TimerLogComponent} from '../timer-log/timer-log.component';
@Component({
  selector: 'app-log-list',
  templateUrl: './log-list.component.html',
  styleUrls: ['../timer-log/timer-log.component.less']
})
export class LogListComponent implements OnInit {
  logs: TimeLog[];
  logSub: Subscription;
  constructor(private logService: LogService) { }

  ngOnInit() {
    this.logSub = this.logService.getLogs().subscribe((logs: TimeLog[]) => {
      this.logs = logs;
    });
  }
  onDeleteLog(log: TimeLog) {
    this.logService.deleteLog(log);

  }

}
