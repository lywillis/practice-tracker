import * as moment from 'moment';
import { TimeLog } from '../../Models/TimeLog';
import { LogService } from '../../services/log.service';
import { Component, OnInit, Input } from '@angular/core';
import { TimerService } from '../../services/timer.service';
import { Subscription } from 'rxjs/Subscription';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-timer-log',
  templateUrl: './timer-log.component.html',
  styleUrls: ['./timer-log.component.less']
})
export class TimerLogComponent implements OnInit {
  @Input() log: TimeLog;
  startDateString: string;
  duration: number;
  timerStatusSub: Subscription;
  constructor() { }

  ngOnInit() {
    const startDate = new Date(this.log.start);
    this.startDateString = moment(startDate).format('ddd MMM D YYYY, hh:mm a');
    this.duration = moment.duration(this.log.ticks, 's').asSeconds();
  }

}
