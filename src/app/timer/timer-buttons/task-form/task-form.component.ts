import { Component, OnInit, Output, EventEmitter } from '@angular/core';


@Component({
  selector: 'app-task-form',
  templateUrl: './task-form.component.html',
  styleUrls: ['../timer-buttons.component.less']
})
export class TaskFormComponent implements OnInit {
  @Output() taskName = new EventEmitter<string>();
  task = '';
  constructor() { }

  ngOnInit() {
  }
  onTaskEntry(formEntry: any) {
    this.taskName.emit(formEntry.task);
    this.task = '';
  }
}
