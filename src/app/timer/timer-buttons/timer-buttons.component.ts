import { Component, OnInit, EventEmitter } from '@angular/core';
import { TimerService } from '../../services/timer.service';
import { Subscription } from 'rxjs/Subscription';
import { LogService } from '../../services/log.service';

@Component({
  selector: 'app-timer-buttons',
  templateUrl: './timer-buttons.component.html',
  styleUrls: ['./timer-buttons.component.less']
})
export class TimerButtonsComponent implements OnInit {
  timerStatusSub: Subscription;
  play: boolean;
  constructor(private timerService: TimerService) { }

  ngOnInit() {
    this.timerStatusSub = this.timerService.timerStatus$.subscribe(status => {
      this.setStatus(status);
    });

  }
  private setStatus(status: any) {
    status.play ? this.play = true : this.play = false;
  }
  startTimer() {
    this.timerService.startTimer();
  }

  pauseTimer() {
    this.timerService.pauseTimer();
  }

  stopTimer() {
    this.timerService.stopTimer();

  }

}
