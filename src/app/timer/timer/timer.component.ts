import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { Observable, Subscription } from 'rxjs';
import { TimerService } from '../../services/timer.service';

@Component({
  selector: 'app-timer',
  templateUrl: './timer.component.html',
  styleUrls: ['./timer.component.less']
})
export class TimerComponent implements OnInit {
  @Output() newTimerEvent$ = new EventEmitter();
  startDate: number;
  // seconds tracker
  ticks = 0;
  start = 0;
  timerStatusSub: Subscription;
  timerSub: Subscription;
  // display clock
  seconds = 0;
  minutes = 0;
  hours = 0;
  startTime: Date;
  constructor(private timerService: TimerService) { }

  ngOnInit() {
    this.timerStatusSub = this.timerService.timerStatus$.subscribe(status => {
      this.handleStatus(status);
    }
    );
  }
  handleStatus(status: any) {
   if (status.play) {
     this.playTimer();
   }
   if (status.pause) {
     this.pauseTimer();
   }
   if (status.stop) {
     this.stopTimer();
   }
  }
  playTimer() {
    this.startDate = Date.now();
    this.timerSub = Observable.timer(1, 10).subscribe(ticks => {
      this.ticks = ticks + this.start;
      this.seconds = this.getSeconds(this.ticks);
      this.minutes = this.getMinutes(this.ticks);
      this.hours = this.getHours(this.ticks);
    });
   }

  pauseTimer() {
    this.start += this.ticks;
    this.timerSub.unsubscribe();
   }

  stopTimer() {
    this.newTimerEvent$.emit({start: this.startDate, duration: this.ticks });
    // clear timer
    this.start = 0;
    this.ticks = 0;
    this.seconds = 0;
    this.minutes = 0;
    this.hours = 0;
    this.timerSub.unsubscribe();
   }

   private getSeconds(ticks: number) {
     return ticks % 60;
   }
   private getMinutes(ticks: number) {
     return Math.floor(ticks / 60) % 60;
   }
   private getHours(ticks: number) {
     return Math.floor((ticks / 60) / 60);
   }

}
