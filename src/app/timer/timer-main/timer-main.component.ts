import { Component, OnInit } from '@angular/core';
import { LogService } from '../../services/log.service';
import { TimeLog } from '../../Models/TimeLog';

@Component({
  selector: 'app-timer-main',
  templateUrl: './timer-main.component.html',
  styleUrls: ['../timer/timer.component.less']
})
export class TimerMainComponent implements OnInit {
  taskName: string;
  constructor(private logService: LogService) { }

  ngOnInit() {
  }

  logTime(timeEvent: any) {
    const timeLog: TimeLog = {start: timeEvent.start, ticks: timeEvent.duration, task: this.taskName};
    this.logService.completeLog(timeLog);
  }
  setTask($event: string) {
    this.taskName = $event;
  }
}
