import { LogService } from './services/log.service';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule} from '@angular/forms';
import { AngularFirestoreModule } from 'angularfire2/firestore';
import { AngularFireModule } from 'angularfire2';
import {RouterModule, Routes} from '@angular/router';

import { AppComponent } from './app.component';
import { environment } from '../environments/environment';
import { TimerService } from './services/timer.service';
import { TimerComponent } from './timer/timer/timer.component';
import { TimerButtonsComponent } from './timer/timer-buttons/timer-buttons.component';
import { TimerLogComponent } from './log/timer-log/timer-log.component';
import { LogListComponent } from './log/log-list/log-list.component';
import { DataModule } from './data/data.module';
import { TaskFormComponent } from './timer/timer-buttons/task-form/task-form.component';
import { TimerMainComponent } from './timer/timer-main/timer-main.component';
import { DataComponent } from './data/data/data.component';

const routes: Routes = [
  { path: '', redirectTo: 'timer', pathMatch: 'full'},
  { path: 'timer', component: TimerMainComponent},
  { path: 'data', component: DataComponent}
];
@NgModule({
  declarations: [
    AppComponent,
    TimerComponent,
    TimerButtonsComponent,
    TimerLogComponent,
    LogListComponent,
    TaskFormComponent,
    TimerMainComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFirestoreModule,
    DataModule,
    RouterModule.forRoot(routes)
  ],
  providers: [TimerService, LogService],
  bootstrap: [AppComponent]
})
export class AppModule { }
