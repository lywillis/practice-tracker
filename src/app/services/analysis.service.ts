import { Injectable, EventEmitter } from '@angular/core';
import { LogService } from './log.service';
import { TimeCategoryEnum } from '../Models/TimeCategoryEnum';
import { TimeLog } from '../Models/TimeLog';
import * as moment from 'moment';

export interface DataSlice {
  timeCategory: TimeCategoryEnum;
  startTime: number;
  endTime: number;
}
@Injectable()
export class AnalysisService {
  monthNames = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
  daysOfWeek = ['Sun', 'Mon', 'Tues', 'Wed', 'Thurs', 'Fri', 'Sat'];
  constructor(private logService: LogService) { }

  public timeCategoryStatus$ = new EventEmitter<DataSlice>();
  private startTime: number;
  private endTime: number;
  private timeCategory: TimeCategoryEnum;

  static aggregateByDay(data: TimeLog[], year: number, month: number) { // for month
    const days = new Date(year, month, 0).getUTCDate(); // number of days in month
    const start = Date.UTC(year, month);
    const end = Date.UTC(year, month, days);
    const result = new Array(days).fill(0);
    for (const log of data) {
      const day = new Date(log.start).getDate();
      result[day - 1] += log.ticks;
    }
    return result;
  }

  static aggregateByDayOfWeek(data: TimeLog[]) {
    const result = new Array(7).fill(0);
    for (const log of data) {
      const day = new Date(log.start).getDay();
      result[day] += log.ticks;
    }
    return result;
  }

  static aggregateByMonth(data: TimeLog[]) { // for year
    const result = new Array(12).fill(0);
    for (const log of data) {
      const month = new Date(log.start).getMonth();
      result[month] += log.ticks;
    }
    return result;
  }
  static aggregateByHour(data: TimeLog[]) {
    const result = new Array(24).fill(0);
    for (const log of data) {
      const hour = new Date(log.start).getHours();
      result[hour] += log.ticks;
    }
    return result;
  }
  getChartTitle(): string {
    const month = new Date(this.startTime).getUTCMonth();
    const year = new Date(this.startTime).getUTCFullYear();
    switch (this.timeCategory) {
      case TimeCategoryEnum.Month: {
        // beginning of month
        return moment({month: month, year: year}).format('MMMM YYYY');
      }
      case TimeCategoryEnum.Year: {
        return year.toString();
      }
      case TimeCategoryEnum.Week: { // first day of week is sunday
        const start = new Date(this.startTime);
        const end = new Date(start.getUTCFullYear(), start.getUTCMonth(), start.getUTCDate() + 6);
        return `${this.monthNames[month]} ${start.getUTCDate()} - ${end.getUTCDate()}`;
      }
      case TimeCategoryEnum.Day: {
        const day = new Date(this.startTime).getUTCDate();
        return `${this.monthNames[month]} ${day}`;
      }
    }
  }

  getLabelsForMonth(year: number, month: number): any[] {
    const days = new Date(year, month, 0).getUTCDate(); // number of days in month
    const result = new Array<number>(days).fill(0);
    return result.map((v, i) => {
      return i + 1;
    });
  }
  getLabelsForYear() {
    return this.monthNames;
  }

  getLabelsForWeek() {
    const date = new Date(this.startTime);
    const start = date.getUTCDate(); // sunday of current week
    const days = new Array(7).fill(0);
    return days.map((v, i) => {
      const newDate = new Date(date.getUTCFullYear(), date.getUTCMonth(), start + i);
      return moment(newDate).format('dd DD');
    });
  }

  getLabelsForDay() {
    const hours = new Array(24).fill(0);
    return hours.map((v, i) => {
      return moment({h: i}).format('hh:mm');
    });
  }

  setTimeCategory(option: TimeCategoryEnum) {
    if (option !== this.timeCategory) {// new category, so reset start and endtimes
      this.startTime = this.getStartTime(option);
      this.endTime = this.getEndTime(option);
    }
    this.timeCategory = option;
    this.timeCategoryStatus$.emit({
      timeCategory: this.timeCategory,
      startTime: this.startTime,
      endTime: this.endTime
    });
  }
  goBack() {
    const start = new Date(this.startTime);
    this.endTime = this.startTime;
    switch (this.timeCategory) {
      case TimeCategoryEnum.Month: { // go back 1 month
        const currMonth = new Date(this.startTime).getUTCMonth();
        this.startTime = Date.UTC(start.getUTCFullYear(), currMonth - 1);
        break;
      }
      case TimeCategoryEnum.Year: {
        this.startTime = Date.UTC(start.getUTCFullYear() - 1, 1);
        break;
      }
      case TimeCategoryEnum.Day: {
        const currDay = start.getUTCDate();
        this.startTime = Date.UTC(start.getUTCFullYear(), start.getUTCMonth(), currDay - 1);
        break;
      }
      case TimeCategoryEnum.Week: {
        const currDay = start.getUTCDate();
        this.startTime = Date.UTC(start.getUTCFullYear(), start.getUTCMonth(), currDay - 7);
        break;
      }
    }
    this.timeCategoryStatus$.emit({
      timeCategory: this.timeCategory,
      startTime: this.startTime,
      endTime: this.endTime
    });
  }
  goForward() {
    const end = new Date(this.endTime);
    this.startTime = this.endTime;
    switch (this.timeCategory) {
      case TimeCategoryEnum.Month: { // go back 1 month
        const currMonth = end.getUTCMonth();
        this.endTime = Date.UTC(end.getUTCFullYear(), currMonth + 1);
        break;
      }
      case TimeCategoryEnum.Year: {
        this.endTime = Date.UTC(end.getUTCFullYear() + 1, 1);
        break;
      }
      case TimeCategoryEnum.Day: {
        const currDay = end.getUTCDate();
        this.endTime = Date.UTC(end.getUTCFullYear(), end.getUTCMonth(), currDay + 1);
        break;
      }
      case TimeCategoryEnum.Week: {
        const currDay = end.getUTCDate();
        this.endTime = Date.UTC(end.getUTCFullYear(), end.getUTCMonth(), currDay + 7);
        break;
      }
    }
    this.timeCategoryStatus$.emit({
      timeCategory: this.timeCategory,
      startTime: this.startTime,
      endTime: this.endTime
    });

  }

  private getStartTime(timeCategory: TimeCategoryEnum): number {
    const now = new Date();
    switch (timeCategory) {
      case TimeCategoryEnum.Month: {
        // beginning of month
        return Date.UTC(now.getUTCFullYear(), now.getUTCMonth());
      }
      case TimeCategoryEnum.Year: {
        // beginning of year
        return Date.UTC(now.getUTCFullYear(), 1);
      }
      case TimeCategoryEnum.Week: { // first day of week is sunday
        const dayOfWeek = now.getUTCDay();
        // number of days you need to subtract to get to sunday
        return Date.UTC(now.getUTCFullYear(), now.getUTCMonth(), now.getUTCDate() - dayOfWeek);
      }
      case TimeCategoryEnum.Day: {
        return Date.UTC(now.getUTCFullYear(), now.getUTCMonth(), now.getUTCDate());
      }
    }
  }
  private getEndTime(timeCategory: TimeCategoryEnum): number {
    const start = new Date(this.getStartTime(timeCategory));
    switch (timeCategory) {
      case TimeCategoryEnum.Month: {
        // beginning of month
        return Date.UTC(start.getUTCFullYear(), start.getUTCMonth() + 1);
      }
      case TimeCategoryEnum.Year: {
        // beginning of year
        return Date.UTC(start.getUTCFullYear() + 1, 1);
      }
      case TimeCategoryEnum.Week: { // first day of week is sunday
        const dayOfWeek = start.getUTCDay();
        // number of days you need to subtract to get to sunday
        return Date.UTC(start.getUTCFullYear(), start.getUTCMonth(), start.getUTCDate() + 7);
      }
      case TimeCategoryEnum.Day: {
        return Date.UTC(start.getUTCFullYear(), start.getUTCMonth(), start.getUTCDate() + 1);
      }
    }
  }
}
