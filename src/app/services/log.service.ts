import { Injectable } from '@angular/core';
import { TimeLog } from '../Models/TimeLog';
import { AngularFirestore, AngularFirestoreCollection} from 'angularfire2/firestore';
import { Http, Headers, Response, URLSearchParams } from '@angular/http';
import {AngularFireDatabase} from 'angularfire2/database';
import * as firebase from 'firebase';
import { Observable, Subscription } from 'rxjs';




@Injectable()
export class LogService {
  logCollection: AngularFirestoreCollection<TimeLog>;
  baseURL = '/logs';
  constructor(private firestore: AngularFirestore) {
    this.logCollection = firestore.collection(this.baseURL, ref => ref.orderBy('start', 'desc'));
   }

  completeLog(log: TimeLog) {
    if (log.task == null) {
      log.task = 'untitled task';
    }
    this.logCollection.doc(log.start.toString()).set(log);
    const addLog = firebase.functions().httpsCallable('addLog');
    addLog(log).then(result => console.log(result));

   } // time log pushed to firebase

  getLogs() { // logs that appear on main log time page
    return this.firestore.collection(this.baseURL, ref => ref.orderBy('start', 'desc').limit(15)).valueChanges();
  }

  deleteLog(log: TimeLog) {
    this.logCollection.doc(log.start.toString()).delete();
  }
  getLogsForTimeRange(start: number, end: number) { // takes in ticks
    return this.firestore.collection<TimeLog>(this.baseURL, ref => ref.where('start', '>=', start).where('start', '<=', end)
    ).valueChanges();
  }
}
