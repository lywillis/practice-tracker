export class TimeLog {
    task?: string;
    start?: number;
    ticks?: number;
}
