// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: 'AIzaSyCvqYYFY9JfjgCVHKN9rZhC299uquc-4pc',
    authDomain: 'time-logger-8dfc2.firebaseapp.com',
    databaseURL: 'https://time-logger-8dfc2.firebaseio.com',
    projectId: 'time-logger-8dfc2',
    storageBucket: 'time-logger-8dfc2.appspot.com',
    messagingSenderId: '461092478506'
  }
};
